<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">

    <modelVersion>4.0.0</modelVersion>

    <groupId>com.fortitudetec</groupId>
    <artifactId>elucidation</artifactId>
    <version>4.1.3-SNAPSHOT</version>

    <packaging>pom</packaging>
    <name>${project.groupId}:${project.artifactId}</name>
    <description>
        Elucidation is a set of tools to allow for the visualization of the connections between microservices that make
        up an environment.
    </description>
    <url>https://gitlab.com/fortitudetec/elucidation-project/elucidation</url>
    <inceptionYear>2018</inceptionYear>

    <organization>
        <name>Fortitude Technologies, LLC</name>
        <url>https://www.fortitudetec.com/</url>
    </organization>

    <developers>
        <developer>
            <name>Chris Rohr</name>
            <organization>Fortitude Technologies, LLC</organization>
            <organizationUrl>https://gitlab.com/fortitudetec</organizationUrl>
            <url>https://gitlab.com/chrisrohr</url>
        </developer>
        <developer>
            <name>Terez Ivy</name>
            <organization>Fortitude Technologies, LLC</organization>
            <organizationUrl>https://gitlab.com/fortitudetec</organizationUrl>
            <url>https://gitlab.com/Terezivy</url>
        </developer>
        <developer>
            <name>Scott Leberknight</name>
            <organization>Fortitude Technologies, LLC</organization>
            <organizationUrl>https://gitlab.com/fortitudetec</organizationUrl>
            <url>https://gitlab.com/sleberknight</url>
        </developer>
    </developers>

    <contributors>
        <contributor>
            <name>John Loizeaux</name>
            <url>https://gitlab.com/loizeaux</url>
        </contributor>
    </contributors>

    <licenses>
        <license>
            <name>MIT License</name>
            <url>http://www.opensource.org/licenses/mit-license.php</url>
            <distribution>repo</distribution>
        </license>
    </licenses>

    <scm>
        <connection>scm:git:https://gitlab.com/fortitudetec/elucidation-project/elucidation.git</connection>
        <developerConnection>scm:git:git@gitlab.com:fortitudetec/elucidation-project/elucidation.git</developerConnection>
        <url>https://gitlab.com/fortitudetec/elucidation-project/elucidation</url>
        <tag>HEAD</tag>
    </scm>

    <distributionManagement>
        <snapshotRepository>
            <id>sonatype-nexus-snapshots</id>
            <name>Sonatype Nexus Snapshots</name>
            <url>${sonatypeOssDistMgmtSnapshotsUrl}</url>
        </snapshotRepository>
        <repository>
            <id>sonatype-nexus-staging</id>
            <name>Nexus Release Repository</name>
            <url>https://oss.sonatype.org/service/local/staging/deploy/maven2/</url>
        </repository>
    </distributionManagement>

    <modules>
        <module>elucidation-bom</module>
        <module>elucidation-common</module>
        <module>elucidation-bundle</module>
        <module>elucidation-client</module>
    </modules>

    <properties>
        <license.licenseName>mit</license.licenseName>

        <sonatypeOssDistMgmtSnapshotsUrl>https://oss.sonatype.org/content/repositories/snapshots/</sonatypeOssDistMgmtSnapshotsUrl>
        <arguments />

        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>

        <!-- sonarqube properties -->
        <sonar.organization>fortitudetec</sonar.organization>
        <sonar.host.url>https://sonarcloud.io</sonar.host.url>
        <sonar.java.source>11</sonar.java.source>
        <sonar.java.target>11</sonar.java.target>

        <!-- dependency versions -->
        <assertj-core.version>3.23.1</assertj-core.version>
        <dropwizard.version>2.1.2</dropwizard.version>
        <jersey.version>2.37</jersey.version>
        <junit5.jupiter.version>5.9.0</junit5.jupiter.version>
        <mockito-core.version>4.8.0</mockito-core.version>

        <!-- plugin versions -->
        <buildnumber-maven-plugin.version>3.0.0</buildnumber-maven-plugin.version>
        <jacoco-maven-plugin.version>0.8.8</jacoco-maven-plugin.version>
        <license-maven-plugin.version>2.0.0</license-maven-plugin.version>
        <maven-clean-plugin.version>3.2.0</maven-clean-plugin.version>
        <maven-compiler-plugin.version>3.10.1</maven-compiler-plugin.version>
        <maven-deploy-plugin.version>3.0.0</maven-deploy-plugin.version>
        <maven-enforcer-plugin.version>3.1.0</maven-enforcer-plugin.version>
        <maven-gpg-plugin.version>3.0.1</maven-gpg-plugin.version>
        <maven-install-plugin.version>3.0.1</maven-install-plugin.version>
        <maven-jar-plugin.version>3.2.2</maven-jar-plugin.version>
        <maven-javadoc-plugin.version>3.4.0</maven-javadoc-plugin.version>
        <maven-project-info-reports-plugin.version>3.4.0</maven-project-info-reports-plugin.version>
        <maven-release-plugin.version>3.0.0-M6</maven-release-plugin.version>
        <maven-resources-plugin.version>3.3.0</maven-resources-plugin.version>
        <maven-site-plugin.version>3.12.0</maven-site-plugin.version>
        <maven-source-plugin.version>3.2.1</maven-source-plugin.version>
        <maven-surefire-plugin.version>3.0.0-M7</maven-surefire-plugin.version>
    </properties>

    <dependencies>

        <!-- Third party testing dependencies -->

        <dependency>
            <groupId>org.assertj</groupId>
            <artifactId>assertj-core</artifactId>
            <version>${assertj-core.version}</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-api</artifactId>
            <version>${junit5.jupiter.version}</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-params</artifactId>
            <version>${junit5.jupiter.version}</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-engine</artifactId>
            <version>${junit5.jupiter.version}</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.mockito</groupId>
            <artifactId>mockito-core</artifactId>
            <version>${mockito-core.version}</version>
            <scope>test</scope>
        </dependency>

    </dependencies>

    <build>

        <pluginManagement><!-- lock down plugins versions to avoid using Maven defaults -->

            <plugins>

                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-site-plugin</artifactId>
                    <version>${maven-site-plugin.version}</version>
                </plugin>

                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-project-info-reports-plugin</artifactId>
                    <version>${maven-project-info-reports-plugin.version}</version>
                </plugin>

                <plugin>
                    <artifactId>maven-clean-plugin</artifactId>
                    <version>${maven-clean-plugin.version}</version>
                </plugin>

                <plugin>
                    <artifactId>maven-enforcer-plugin</artifactId>
                    <version>${maven-enforcer-plugin.version}</version>
                </plugin>

                <!-- see http://maven.apache.org/ref/current/maven-core/default-bindings.html#Plugin_bindings_for_jar_packaging -->
                <plugin>
                    <artifactId>maven-resources-plugin</artifactId>
                    <version>${maven-resources-plugin.version}</version>
                </plugin>

                <plugin>
                    <artifactId>maven-compiler-plugin</artifactId>
                    <version>${maven-compiler-plugin.version}</version>
                    <configuration>
                        <release>11</release>
                    </configuration>
                </plugin>

                <plugin>
                    <artifactId>maven-surefire-plugin</artifactId>
                    <version>${maven-surefire-plugin.version}</version>
                    <configuration>
                        <systemProperties>
                            <java.awt.headless>true</java.awt.headless>
                        </systemProperties>
                    </configuration>
                </plugin>

                <plugin>
                    <artifactId>maven-jar-plugin</artifactId>
                    <version>${maven-jar-plugin.version}</version>
                    <configuration>
                        <archive>
                            <manifest>
                                <addDefaultImplementationEntries>true</addDefaultImplementationEntries>
                            </manifest>
                            <manifestEntries>
                                <Implementation-Build>$\{buildNumber}</Implementation-Build>
                            </manifestEntries>
                        </archive>
                    </configuration>
                </plugin>

                <plugin>
                    <artifactId>maven-install-plugin</artifactId>
                    <version>${maven-install-plugin.version}</version>
                </plugin>

                <plugin>
                    <artifactId>maven-deploy-plugin</artifactId>
                    <version>${maven-deploy-plugin.version}</version>
                </plugin>

                <plugin>
                    <artifactId>maven-release-plugin</artifactId>
                    <version>${maven-release-plugin.version}</version>
                    <configuration>
                        <mavenExecutorId>forked-path</mavenExecutorId>
                        <useReleaseProfile>false</useReleaseProfile>
                        <arguments>${arguments} -Psonatype-oss-release</arguments>
                        <tagNameFormat>v@{project.version}</tagNameFormat>
                    </configuration>
                </plugin>

                <plugin>
                    <groupId>org.codehaus.mojo</groupId>
                    <artifactId>license-maven-plugin</artifactId>
                    <version>${license-maven-plugin.version}</version>
                </plugin>

                <plugin>
                    <groupId>org.codehaus.mojo</groupId>
                    <artifactId>buildnumber-maven-plugin</artifactId>
                    <version>${buildnumber-maven-plugin.version}</version>
                    <executions>
                        <execution>
                            <phase>validate</phase>
                            <goals>
                                <goal>create</goal>
                            </goals>
                        </execution>
                    </executions>
                    <configuration>
                        <shortRevisionLength>8</shortRevisionLength>
                        <doCheck>false</doCheck>
                        <doUpdate>false</doUpdate>
                    </configuration>
                </plugin>

                <plugin>
                    <groupId>org.jacoco</groupId>
                    <artifactId>jacoco-maven-plugin</artifactId>
                    <version>${jacoco-maven-plugin.version}</version>
                    <executions>
                        <execution>
                            <id>prepare-agent</id>
                            <phase>initialize</phase>
                            <goals>
                                <goal>prepare-agent</goal>
                            </goals>
                        </execution>
                        <execution>
                            <id>report</id>
                            <goals>
                                <goal>report-aggregate</goal>
                            </goals>
                            <phase>verify</phase>
                        </execution>
                    </executions>
                </plugin>

            </plugins>

        </pluginManagement>

        <plugins>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-enforcer-plugin</artifactId>
                <version>${maven-enforcer-plugin.version}</version>
                <executions>
                    <execution>
                        <id>enforce</id>
                        <configuration>
                            <rules>
                                <dependencyConvergence />
                                <bannedDependencies>
                                    <excludes>
                                        <!-- As recommended from slf4j guide, exclude commons-logging -->
                                        <exclude>commons-logging:commons-logging</exclude>
                                        <!-- Ensure only the slf4j binding for logback is on the classpath -->
                                        <exclude>log4j:log4j</exclude>
                                        <!-- This should not exist as it will force slf4j calls to be delegated to JUL -->
                                        <exclude>org.slf4j:slf4j-jdk14</exclude>
                                        <!-- This should not exist as it will force slf4j calls to be delegated to log4j -->
                                        <exclude>org.slf4j:slf4j-log4j12</exclude>
                                    </excludes>
                                </bannedDependencies>
                                <requireMavenVersion>
                                    <version>3.3.9</version>
                                </requireMavenVersion>
                            </rules>
                        </configuration>
                        <goals>
                            <goal>enforce</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>

    </build>

    <profiles>
        <profile>
            <id>sonatype-oss-release</id>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-source-plugin</artifactId>
                        <version>${maven-source-plugin.version}</version>
                        <executions>
                            <execution>
                                <id>attach-sources</id>
                                <goals>
                                    <goal>jar-no-fork</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-javadoc-plugin</artifactId>
                        <version>${maven-javadoc-plugin.version}</version>
                        <executions>
                            <execution>
                                <id>attach-javadocs</id>
                                <goals>
                                    <goal>jar</goal>
                                </goals>
                            </execution>
                        </executions>
                        <configuration>
                            <tags>
                                <tag>
                                    <name>apiNote</name>
                                    <placement>a</placement>
                                    <head>API Note:</head>
                                </tag>
                                <tag>
                                    <name>implSpec</name>
                                    <placement>a</placement>
                                    <head>Implementation Requirements:</head>
                                </tag>
                                <tag>
                                    <name>implNote</name>
                                    <placement>a</placement>
                                    <head>Implementation Note:</head>
                                </tag>
                                <tag><name>param</name></tag>
                                <tag><name>return</name></tag>
                                <tag><name>throws</name></tag>
                                <tag><name>since</name></tag>
                                <tag><name>version</name></tag>
                                <tag><name>serialData</name></tag>
                                <tag><name>see</name></tag>
                            </tags>
                        </configuration>
                    </plugin>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-gpg-plugin</artifactId>
                        <version>${maven-gpg-plugin.version}</version>
                        <executions>
                            <execution>
                                <id>sign-artifacts</id>
                                <phase>verify</phase>
                                <goals>
                                    <goal>sign</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>
    </profiles>

</project>
