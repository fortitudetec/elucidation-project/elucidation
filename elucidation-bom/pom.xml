<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">

    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>com.fortitudetec</groupId>
        <artifactId>elucidation</artifactId>
        <version>4.1.3-SNAPSHOT</version>
    </parent>

    <artifactId>elucidation-bom</artifactId>
    <version>4.1.3-SNAPSHOT</version>
    <packaging>pom</packaging>
    <name>Elucidation BOM</name>
    <description>Bill of materials to use consistent set of versions across Elucidation modules</description>

    <properties>
        <caffeine.version>3.1.1</caffeine.version>
        <checker-qual.version>3.25.0</checker-qual.version>
        <commons-collections4.version>4.4</commons-collections4.version>
        <commons-lang3.version>3.12.0</commons-lang3.version>
        <commons-text.version>1.9</commons-text.version>
        <error-prone-annotations.version>2.15.0</error-prone-annotations.version>
        <guava.version>31.1-jre</guava.version>
        <h2.version>2.1.214</h2.version>
        <hibernate-validator.version>6.2.4.Final</hibernate-validator.version>
        <hk2-utils.version>2.6.1</hk2-utils.version>
        <jackson.version>2.13.4</jackson.version>
        <jakarta.activation-api.version>1.2.2</jakarta.activation-api.version>
        <jakarta-el.version>3.0.4</jakarta-el.version>
        <jdbi3.version>3.32.0</jdbi3.version>
        <jersey.version>2.37</jersey.version>
        <jetty.version>9.4.48.v20220622</jetty.version>
        <joda-time.version>2.11.1</joda-time.version>
        <kiwi.version>2.2.0</kiwi.version>
        <kiwi-test.version>2.2.1</kiwi-test.version>
        <logback-classic.version>1.2.11</logback-classic.version>
        <logback-core.version>1.2.11</logback-core.version>
        <lombok.version>1.18.24</lombok.version>
        <postgresql.version>42.5.0</postgresql.version>
        <slf4j-api.version>1.7.32</slf4j-api.version>
        <snakeyaml.version>1.32</snakeyaml.version>
    </properties>

    <dependencyManagement>

        <dependencies>

            <dependency>
                <groupId>com.github.ben-manes.caffeine</groupId>
                <artifactId>caffeine</artifactId>
                <version>${caffeine.version}</version>
            </dependency>

            <dependency>
                <groupId>org.checkerframework</groupId>
                <artifactId>checker-qual</artifactId>
                <version>${checker-qual.version}</version>
            </dependency>

            <dependency>
                <groupId>com.google.errorprone</groupId>
                <artifactId>error_prone_annotations</artifactId>
                <version>${error-prone-annotations.version}</version>
            </dependency>

            <dependency>
                <groupId>ch.qos.logback</groupId>
                <artifactId>logback-classic</artifactId>
                <version>${logback-classic.version}</version>
            </dependency>

            <dependency>
                <groupId>ch.qos.logback</groupId>
                <artifactId>logback-core</artifactId>
                <version>${logback-core.version}</version>
            </dependency>

            <dependency>
                <groupId>com.fasterxml.jackson</groupId>
                <artifactId>jackson-bom</artifactId>
                <version>${jackson.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>

            <dependency>
                <groupId>jakarta.activation</groupId>
                <artifactId>jakarta.activation-api</artifactId>
                <version>${jakarta.activation-api.version}</version>
            </dependency>

            <dependency>
                <groupId>org.glassfish.jersey</groupId>
                <artifactId>jersey-bom</artifactId>
                <version>${jersey.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>

            <dependency>
                <groupId>org.eclipse.jetty</groupId>
                <artifactId>jetty-bom</artifactId>
                <version>${jetty.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>

            <dependency>
                <groupId>joda-time</groupId>
                <artifactId>joda-time</artifactId>
                <version>${joda-time.version}</version>
            </dependency>

            <dependency>
                <groupId>com.google.guava</groupId>
                <artifactId>guava</artifactId>
                <version>${guava.version}</version>
                <exclusions>
                    <exclusion>
                        <groupId>org.checkerframework</groupId>
                        <artifactId>checker-qual</artifactId>
                    </exclusion>
                </exclusions>
            </dependency>

            <dependency>
                <groupId>com.h2database</groupId>
                <artifactId>h2</artifactId>
                <version>${h2.version}</version>
            </dependency>

            <dependency>
                <groupId>io.dropwizard</groupId>
                <artifactId>dropwizard-core</artifactId>
                <version>${dropwizard.version}</version>
                <exclusions>
                    <exclusion>
                        <groupId>com.github.ben-manes.caffeine</groupId>
                        <artifactId>caffeine</artifactId>
                    </exclusion>
                </exclusions>
            </dependency>

            <dependency>
                <groupId>io.dropwizard</groupId>
                <artifactId>dropwizard-db</artifactId>
                <version>${dropwizard.version}</version>
            </dependency>

            <dependency>
                <groupId>io.dropwizard</groupId>
                <artifactId>dropwizard-jdbi3</artifactId>
                <version>${dropwizard.version}</version>
            </dependency>

            <dependency>
                <groupId>io.dropwizard</groupId>
                <artifactId>dropwizard-jersey</artifactId>
                <version>${dropwizard.version}</version>
            </dependency>

            <dependency>
                <groupId>io.dropwizard</groupId>
                <artifactId>dropwizard-migrations</artifactId>
                <version>${dropwizard.version}</version>
            </dependency>

            <dependency>
                <groupId>io.dropwizard</groupId>
                <artifactId>dropwizard-testing</artifactId>
                <version>${dropwizard.version}</version>
                <exclusions>
                    <exclusion>
                        <groupId>jakarta.activation</groupId>
                        <artifactId>jakarta.activation-api</artifactId>
                    </exclusion>
                    <exclusion>
                        <groupId>jakarta.servlet</groupId>
                        <artifactId>jakarta.servlet-api</artifactId>
                    </exclusion>
                    <exclusion>
                        <groupId>jakarta.xml.bind</groupId>
                        <artifactId>jakarta.xml.bind-api</artifactId>
                    </exclusion>
                </exclusions>
            </dependency>

            <dependency>
                <groupId>org.apache.commons</groupId>
                <artifactId>commons-collections4</artifactId>
                <version>${commons-collections4.version}</version>
            </dependency>

            <dependency>
                <groupId>org.apache.commons</groupId>
                <artifactId>commons-lang3</artifactId>
                <version>${commons-lang3.version}</version>
            </dependency>

            <dependency>
                <groupId>org.apache.commons</groupId>
                <artifactId>commons-text</artifactId>
                <version>${commons-text.version}</version>
            </dependency>

            <dependency>
                <groupId>org.glassfish</groupId>
                <artifactId>jakarta.el</artifactId>
                <version>${jakarta-el.version}</version>
            </dependency>

            <dependency>
                <groupId>org.glassfish.hk2</groupId>
                <artifactId>hk2-utils</artifactId>
                <version>${hk2-utils.version}</version>
                <exclusions>
                    <exclusion>
                        <groupId>jakarta.annotation</groupId>
                        <artifactId>jakarta.annotation-api</artifactId>
                    </exclusion>
                </exclusions>
            </dependency>

            <dependency>
                <groupId>org.glassfish.jersey.inject</groupId>
                <artifactId>jersey-hk2</artifactId>
                <version>${jersey.version}</version>
                <exclusions>
                    <exclusion>
                        <groupId>org.javassist</groupId>
                        <artifactId>javassist</artifactId>
                    </exclusion>
                </exclusions>
            </dependency>

            <dependency>
                <groupId>org.glassfish.jersey.core</groupId>
                <artifactId>jersey-client</artifactId>
                <version>${jersey.version}</version>
            </dependency>

            <dependency>
                <groupId>org.hibernate.validator</groupId>
                <artifactId>hibernate-validator</artifactId>
                <version>${hibernate-validator.version}</version>
                <exclusions>
                    <exclusion>
                        <groupId>com.fasterxml</groupId>
                        <artifactId>classmate</artifactId>
                    </exclusion>
                </exclusions>
            </dependency>

            <dependency>
                <groupId>org.jdbi</groupId>
                <artifactId>jdbi3-bom</artifactId>
                <version>${jdbi3.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>

            <dependency>
                <groupId>org.postgresql</groupId>
                <artifactId>postgresql</artifactId>
                <version>${postgresql.version}</version>
            </dependency>

            <dependency>
                <groupId>org.projectlombok</groupId>
                <artifactId>lombok</artifactId>
                <version>${lombok.version}</version>
            </dependency>

            <dependency>
                <groupId>org.slf4j</groupId>
                <artifactId>slf4j-api</artifactId>
                <version>${slf4j-api.version}</version>
            </dependency>

            <dependency>
                <groupId>org.kiwiproject</groupId>
                <artifactId>kiwi</artifactId>
                <version>${kiwi.version}</version>
            </dependency>

            <dependency>
                <groupId>org.kiwiproject</groupId>
                <artifactId>kiwi-test</artifactId>
                <version>${kiwi-test.version}</version>
            </dependency>

            <dependency>
                <groupId>org.glassfish.jersey.test-framework</groupId>
                <artifactId>jersey-test-framework-core</artifactId>
                <version>${jersey.version}</version>
            </dependency>

            <dependency>
                <groupId>org.yaml</groupId>
                <artifactId>snakeyaml</artifactId>
                <version>${snakeyaml.version}</version>
            </dependency>

        </dependencies>

    </dependencyManagement>

</project>
